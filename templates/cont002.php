<style>
  table {
    border: 1px solid #000;
    width: 100%
  }

  tr>td {
    background-color: #E8E8E8;
    border-bottom: 1px solid #000;
    border-right: 1px solid #000;
    font-weight: bold;
    width: 160px;
  }

  .tb-empty{
    border: 0px solid black
  }

  .tb-empty tr>td{
    background-color: #ffffff;
    border: 0px
  }

  p {
    text-align: justify
  }

  .container {
    width: 100%;
  }

  .col-6 {
    width: 40%;
  }

  .col-6 > p {
    text-align: center !important
  }
</style>

<!-- Selector de sexo -->
<?php
$arrendador = ($storageC->{'arrendadorSexo'} == 'm') ? 'ARRENDADOR' : 'ARRENDADORA';
$arrendatario = ($storageC->{'arrendatarioSexo'} == 'm') ? 'ARRENDATARIO' : 'ARRENDATARIA';
$coarrendador = (!empty($storageC->{'coarrendatario1Select'}) && $storageC->{'coarrendatario1Select'} == 's') ? true : false ;
?>

<p style="text-align: center"><strong>CONTRATO DE ARRENDAMIENTO DE BIEN INMUEBLE DE VIVIENDA URBANA</strong></p>
<!-- Inicio ARRENDADOR -->
<table style="border-collapse: collapse; margin-bottom: 5px">
  <tr>
    <td><?php echo $arrendador ?></td>
    <td><?php echo $storageC->{'arrendadorNombre'} ?></td>
  </tr>
  <tr>
    <td>IDENTIFICACIÓN</td>
    <td><?php echo $storageC->{'arrendadorTipoDocumento'} . ': ' . $storageC->{'arrendadorNumeroDocumento'} ?></td>
  </tr>
  <tr>
    <td>CORREO ELECTRÓNICO</td>
    <td><?php echo $storageC->{'arrendadorCorreo'} ?></td>
  </tr>
  <tr>
    <td>TELÉFONO</td>
    <td><?php echo $storageC->{'arrendadorCelular'} ?></td>
  </tr>
  <tr>
    <td>DIRECCIÓN</td>
    <td><?php echo $storageC->{'arrendadorDireccion'} ?></td>
  </tr>
</table>
<!-- Fin ARRENDADOR -->
</br>
<!-- Inicio ARRENDATARIO -->
<table style="border-collapse: collapse; margin-bottom: 5px">
  <tr>
    <td><?php echo $arrendatario ?></td>
    <td><?php echo $storageC->{'arrendatarioNombre'} ?></td>
  </tr>
  <tr>
    <td>IDENTIFICACIÓN</td>
    <td><?php echo $storageC->{'arrendatarioTipoDocumento'} . ': ' . $storageC->{'arrendatarioNumeroDocumento'} ?></td>
  </tr>
  <tr>
    <td>CORREO ELECTRÓNICO</td>
    <td><?php echo $storageC->{'arrendatarioCorreo'} ?></td>
  </tr>
  <tr>
    <td>TELÉFONO</td>
    <td><?php echo $storageC->{'arrendatarioCelular'} ?></td>
  </tr>
  <tr>
    <td>DIRECCIÓN</td>
    <td><?php echo $storageC->{'arrendatarioDireccion'} ?></td>
  </tr>
</table>
<!-- Fin ARRENDATARIO -->
</br>
<!-- Inicio COARRENDATARIO 1 -->
<?php
if ( $coarrendador ) {
?>
  <table style="border-collapse: collapse; margin-bottom: 5px"">
  <tr>
    <td>COARRENDATARIO</td>
    <td><?php echo $storageC->{'coarrendatario1Nombre'} ?></td>
  </tr>
  <tr>
    <td>IDENTIFICACIÓN</td>
    <td><?php echo $storageC->{'coarrendatario1TipoDocumento'} . ': ' . $storageC->{'coarrendatario1NumeroDocumento'} ?></td>
  </tr>
  <tr>
    <td>CORREO ELECTRÓNICO</td>
    <td><?php echo $storageC->{'coarrendatario1Correo'} ?></td>
  </tr>
  <tr>
    <td>TELÉFONO</td>
    <td><?php echo $storageC->{'coarrendatario1Celular'} ?></td>
  </tr>
  <tr>
    <td>DIRECCIÓN</td>
    <td><?php echo $storageC->{'coarrendatario1Direccion'} ?></td>
  </tr>
</table>
<?php
}
?>
<!-- Fin COARRENDATARIO 1 -->
</br>
<p><strong>PRIMERA. FECHA DE INICIACIÓN, VIGENCIA, PRÓRROGA Y PREAVISO DEL CONTRATO DE ARRENDAMIENTO.</strong> El término de duración de este contrato es de <?php echo $storageC->{'contratoDuracion'} ?> meses, contados a partir del día <?php echo $storageC->{'contratoFechaInicio'} ?> hasta la fecha de terminación <?php echo $storageC->{'contratoFechaFin'} ?>. El contrato de arrendamiento de vivienda urbana se entenderá prorrogado en iguales condiciones y por el mismo término inicial, siempre que cada una de las partes haya cumplido con las obligaciones a su cargo y, que el arrendatario, se avenga a los reajustes de la renta autorizados en esta ley. Las partes acuerdan un preaviso de <?php echo $storageC->{'contratoFechaAntelacion'} ?> meses de anteleción a la terminación del contrato de arrendamiento.</p>
</br>
<p><strong>SEGUNDA. DIRECCIÓN DEL INMUEBLE.</strong> El bien inmueble objeto del contrato de arrendamiento se identifica con la Dirección <?php echo $storageC->{'direccionInmueble'} ?> de <?php echo $storageC->{'ciudadInmueble'} ?>.</p>
</br>
<p><strong>TERCERA. DESTINACIÓN.</strong> <?php echo 'El ' . $arrendatario ?> se obliga a usar el inmueble para VIVIENDA FAMILIAR, no podrá darle otro uso, ni ceder ni transferir el arrendamiento sin la autorización escrita del ARRENDADOR. El incumplimiento de esta cláusula dará derecho para dar por terminado el contrato y exigir la entrega del inmueble o, en caso de cesión o subarriendo abusivos, celebrar un nuevo contrato con los usuarios reales, sin necesidad de requerimientos judiciales o privados, a los cuales renuncia <?php echo 'el ' . $arrendatario ?>. De acuerdo con lo estipulado con la Ley 820 de 2003. <ins><?php echo 'El ' . $arrendatario ?> pagará <?php echo 'al ' . $arrendador ?> una suma equivalente al valor de <?php echo $storageC->{'nCanonesIncumplimiento'} ?> cánones mensuales de arriendo por incumplimiento de la destinación del inmueble.</ins></p>
</br>
<?php
$msg = '';
switch ($storageC->{'tipoInmueble'}) {
  case 'cs':
    $msg = 'la casa';
    break;
  case 'apto':
    $msg = 'el apartamento';
    break;
  case 'grj':
    $msg = 'el garaje';
    break;
  case 'hab':
    $msg = 'la habitación';
    break;
}
?>
<p><strong>CUARTA. INVENTARIO DEL INMUEBLE A ARRENDAR.</strong> EL ARRENDATARIO declara que ha recibido <?php echo $msg . ' con ' . $storageC->{'metrosCuadrados'} ?> mt2 en estado de funcionamiento y de conservación las instalaciones para uso de los servicios públicos del Inmueble, que se abstendrá de modificarlas sin permiso previo y escrito del ARRENDADOR y que responderá por daños de los reglamentos de las correspondientes empresas de servicios públicos. Asimismo, declara que ha recibido el inmueble objeto de este contrato en la fecha indicada y en la cláusula FECHA DE INICIACIÓN Y VIGENCIA, en buen estado de servicio y presentación, en el estado y que se obliga a cuidarlo, conservarlo y mantenerlo y, que <strong>EN EL MISMO ESTADO LO RESTITUIRÁ</strong>, salvo el deterioro por el paso del tiempo y su uso legítimo.</ins></p>
</br>
<p><strong>QUINTO. PAGO DE CÁNONES.</strong></p>
</br>
<p><strong>PARÁGRAFO PRIMERO.</strong> El incumplimiento del ARRENDATARIO en el pago del canon de arrendamiento, dentro de los plazos y fechas estipuladas, dará lugar a la terminación del contrato y a la devolución inmediata del inmueble al ARRENDADOR, sin perjuicio del pago de las indemnizaciones y penalidades a que haya lugar.</p>
</br>
<?php
$siCoarrendatario = ($coarrendador) ? 'y/o CO-ARRENDATARIO' : '';
?>
<p><strong>PARÁGRAFO SEGUNDO.</strong> En cualquier evento de mora o retardo en el cumplimiento de las obligaciones a cargo del ARRENDATARIO <?php echo $siCoarrendatario; ?>, EL ARRENDADOR queda facultado para exigir, de aquellos el pago de los honorarios de abogado y demás gastos de cobranza judicial y/o extrajudicial. </p>
</br>
<?php
$tipoPago = ($storageC->{'tipoDePago'} == 'a') ? 'de manera mensual, anticipada' : 'mes vencido';
$medioDePago = '';
if ($storageC->{'contratoMedioDePago'} == 'c') {
  $a = ($storageC->{'cBancoTipo'} == 'a') ? 'de ahorros' : 'corriente';
  $b = $storageC->{'cBancoNumero'};
  //$c = $storageC->{'cBancoEntidad'};
  $c = '';
  for ($i = 0; $i < $lengthJsonBank; $i++) {
    if ($readJsonbank[$i]['bankCode'] == $storageC->{'cBancoEntidad'}) { //obtener valor iguales a la ref. obtenida
        $c = $readJsonbank[$i]['bankName']; //Precio del contrato
    }
  }
  $d = $storageC->{'cBancoTitular'};
  $medioDePago = 'depósito en cuenta ' . $a . ' número ' . $b . ' de ' . $c . ' a nombre de ' . $d;
} else {
  $medioDePago = 'efectivo';
}
?>
<p><strong>QUINTA. REAJUSTES DEL CANON DE ARRENDAMIENTO.</strong> El precio mensual del arrendamiento, que actualmente equivale a la suma de <?php echo '$ ' . number_format($storageC->{'contratoValorMensual'}) . ' (' . convertirNumeroLetra($storageC->{'contratoValorMensual'}) . 'PESOS)' ?>, precio que el ARRENDATARIO pagará <?php echo $tipoPago; ?> y a través de <?php echo $medioDePago; ?>. Una vez que se cumplan <?php echo convertirNumeroLetra($storageC->{'contratoDuracion'}) . ' (' . $storageC->{'contratoDuracion'} . ')' ?> meses, se reajustará en un porcentaje máximo o igual al ciento por ciento (100%) del incremento que haya tenido el Índice de Precios al Consumidor (IPC), en el año inmediatamente anterior a aquel en que deba efectuarse el respectivo reajuste del canon o en su defecto, lo que determine el gobierno.</p>
</br>
<p><strong>CUARTA. OBLIGACIONES DEL ARRENDADOR.</strong></p>
<ol>
  <li>Entregar al ARRENDATARIO, en la fecha convenida o en el momento de la celebración del contrato, los inmuebles dados en arrendamiento en buen estado de servicio, seguridad y sanidad, y poner a su disposición los servicios, cosas o usos conexos y los adicionales convenidos.</li>
  <li>Mantener en los inmuebles los servicios, cosas o usos conexos en buen estado de servir para el fin convenido.</li>
  <li>Suministrar copia del presente contrato al ARRENDATARIO, cuyas firmas sean originales, dentro de los quince (15) días siguientes a su celebración.</li>
  <li>Las demás obligaciones consagradas para en las normas vigentes Ley 820 de 2003.</li>
  <?php if ($storageC->{'mascostasSelect'} == 's') : ?>
    <li>El Arrendador permite la tenencia de Mascotas con condiciones contempladas en las obligaciones del arrendatario.</li>
  <?php endif; ?>
  <?php if ($storageC->{'cesionSelect'} == 's') : ?>
    <li>Permite la Cesión del contrato de arrendamiento.</li>
  <?php endif; ?>
  <?php if ($storageC->{'subarriendoSelect'} == 's') : ?>
    <li>Permite el Subarriendo del contrato de arrendamiento.</li>
  <?php endif; ?>
</ol>
</br>
<p><strong>QUINTA.OBLIGACIONES DEL ARRENDATARIO/RIA.</strong></p>
<ol>
  <li>Pagar el precio total del arrendamiento dentro del plazo y en el lugar estipulado en el presente contrato dentro de los tres primeros días siguientes al vencimiento del mismo.</li>
  <li>Cuidar el inmueble recibido en arrendamiento. En caso de daños o deterioros distintos a los derivados del uso normal o de la acción del tiempo, que fueren imputables al mal uso del inmueble o a su propia culpa, deberá efectuar oportunamente y por su cuenta las reparaciones o sustituciones necesarias.</li>
  <li>Cumplir las buenas costumbres y las dispuestas en el Código de Polícia.</li>
  <li>Abstenerse de guardar sustancias explosivas o perjudiciales para la conservación, seguridad o higiene del inmueble.</li>
  <li>Abstenerse de realizar actividades prohibidas por las leyes y las buenas costumbres.</li>
  <li>Cumplir con las demás obligaciones consagradas por las normas vigente</li>
  <?php if ($storageC->{'mascostasSelect'} == 's') : ?>
    <li>Compromiso de hacerse cargo de los daños que realice la Mascotas, además de abstenerse de dejarla en el balcón y mantenerla en condicciones de existencia como un animal sintiente. </li>
  <?php endif; ?>
</ol>
</br>
<p><strong>SEXTA.- TERMINACIÓN DEL CONTRATO.</strong> Las partes acuerdan terminar el presente contrato el <?php echo $storageC->{'contratoFechaFin'} ?> o el año hasta el cual se haya prorrogado, fecha en la cual EL ARRENDATARIO restituirá el inmueble arrendado por medio del presente contrato, siempre y cuando EL ARRENDATARIO haya comunicado al correo electrónico (<?php echo $storageC->{'arrendadorCorreo'} ?>) del ARRENDADOR, de su intención de darlo por terminado, con una antelación no menor a <?php echo $storageC->{'contratoFechaAntelacion'} ?> mes, de lo contrario, se entenderá prorrogado en la integridad este CONTRATO DE ARRENDAMIENTO.</p>
</br>
<p><strong>PARÁGRAFO PRIMERO.</strong> La terminación del contrato, por cualquiera de las partes, en fecha distinta a las mencionadas en esta cláusula o sin cumplir con el preaviso mencionado, obliga a la parte que termine el contrato a indemnizar a la otra parte, con una suma equivalente a <?php echo $storageC->{'nCanonesIncumplimiento'} ?> cánones mensuales de arrendamiento, sin perjuicio de lo estipulado en la Cláusula Novena (Cláusula Penal).</p>
</br>
<p><strong>OCTAVA.- REPARACIONES Y MEJORAS.</strong> Las reparaciones, variaciones y reformas efectuadas por EL ARRENDATARIO al inmueble, serán por cuenta de ésta y requerirán previa autorización escrita del ARRENDADOR para desarrollarlas, entendiendo que de cualquier forma aquellas accederán al inmueble, sin lugar a indemnización para quien las efectuó. Las reparaciones aquí contempladas se conocen como REPARACIONES LOCATIVAS, a diferencia de aquellas contempladas en el numeral 2 de la Cláusula Cuarta del presente contrato y el artículo 1985 del Código Civil.</p>
</br>
<p><strong>NOVENA.- CLÁUSULA PENAL.</strong> En el evento de incumplimiento por cualquiera de las partes a las obligaciones a su cargo, contenidas en este contrato o en la ley, la parte incumplida deberá pagar a la otra parte una suma equivalente a dos (2) cánones mensuales de arrendamiento vigentes en la fecha del incumplimiento, a título de pena O <?php echo $storageC->{'nCanonesIncumplimiento'} ?> sin perjuicio de las demás obligaciones derivadas del presente contrato y de las acciones legales a que haya lugar.</p>
</br>
<!-- <p><strong>DÉCIMA PRIMERA. ARRAS</strong></p>
</br>
<p><strong>ARRAS CONFIRMATORIA.</strong> Las partes ARRENDADOR y ARRENDATARIO establecen como parte del precio o como señal de quedar convenidos los contrates por el valor de $ (xxxxxx) (valor en letras).</p>
</br>
<p><strong>1. ARRAS DE RETRACTO</strong> Las partes ARRENDADOR y ARRENDATARIO establecer la celebración o ejecución por el valor de $ (xxxxxx) (valor en letras).</p>
</br>
<p><strong>2. ARRAS CONFIRMATORIA PENAL</strong> Las partes ARRENDADOR y ARRENDATARIO establecer la celebración o ejecución por el valor de $ (xxxxxx) (valor en letras).</p>
</br> -->
<p><strong>DÉCIMA PRIMERA. GARANTÍA DE SERVICIOS PÚBLICOS</strong> La garantía o depósito, en ningún caso, podrá exceder el valor de los servicios públicos correspondientes al cargo fijo, al cargo por aportes de conexión y al cargo por unidad de consumo, correspondiente a dos (2) períodos consecutivos de facturación, de conformidad con lo establecido en el artículo 18 de la Ley 689 de 2001. La garantía deberá realizarse en cada una de las empresas de servicios públicos y el ARRENDADOR no puede quedarse con esta garantía.</p>
</br>
<p><strong>DÉCIMA SEGUNDA. ABANDONO DEL INMUEBLE</strong> En caso de abandono del inmueble por parte del ARRENDATARIO faculta expresamente al ARRENDADOR o a quien los represente, para que accedan al inmueble objeto del presente contrato y recuperen la tenencia del mismo, con el diligenciamiento del Acta de Restitución privada, judicial o extrajudicial, suscrita por aquellos, con dos (2) testigos hábiles con anotación clara del estado en que se encuentre, los faltantes al inventario y los conceptos adeudados que quedaren pendientes como consecuencia del abandono e incumplimiento.</p>
</br>
<p><strong>DÉCIMA TERCERA. RESTITUCIÓN DEL INMUEBLE</strong> Las partes ARRENDADOR Y ARRENDATARIO acuerdan que en la circunstancia de cualquier conflicto acueden a la solución mediante cualquiera de los mecanismos alternativos de conflictos.</p>
</br>
<!-- <p><strong>DÉCIMA SEXTA.</strong></p> -->
</br>
<p>Para constancia se firma entre las partes el <?php echo $storageC->{'contratoFechaFirma'} ?></p>
</br>
<p>Para efectos de recibir notificaciones judiciales y extrajudiciales, las partes, al suscribir este contrato procede a indicar sus respectivas direcciones.</p>
</br>
<table style="border-collapse: collapse; margin-bottom: 5px; margin-top: 50px" class="tb-empty">
  <tr>
    <td style="text-transform: uppercase;"><?php echo $storageC->{'arrendadorNombre'} ?></td>
    <td style="text-transform: uppercase;"><?php echo $storageC->{'arrendatarioNombre'} ?></td>
  </tr>
  <tr>
    <td style="size: 12px;"><?php echo $arrendador ?></td>
    <td style="size: 12px;"><?php echo $arrendatario ?></td>
  </tr>
  <tr>
    <td style="size: 12px;"><?php echo $storageC->{'arrendadorTipoDocumento'} . ': ' . $storageC->{'arrendadorNumeroDocumento'} ?></td>
    <td style="size: 12px;"><?php echo $storageC->{'arrendatarioTipoDocumento'} . ': ' . $storageC->{'arrendatarioNumeroDocumento'} ?></td>
  </tr>
</table>
<?php
if ( $coarrendador ) {
?>
<table style="border-collapse: collapse; margin-bottom: 5px; margin-top: 50px" class="tb-empty">
  <tr>
    <td style="text-transform: uppercase;"><?php echo $storageC->{'coarrendatario1Nombre'} ?></td>
  </tr>
  <tr>
    <td style="size: 12px;"><?php echo 'COARRENDATARIO' ?></td>
  </tr>
  <tr>
    <td style="size: 12px;"><?php echo $storageC->{'coarrendatario1TipoDocumento'} . ': ' . $storageC->{'coarrendatario1NumeroDocumento'} ?></td>
  </tr>
</table>
<?php }