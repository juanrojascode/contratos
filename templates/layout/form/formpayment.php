<?php
$DesdeLetra = "a";
$HastaLetra = "z";
$DesdeNumero = 1;
$HastaNumero = 9;
$fileName = '';

for ($i = 0; $i < 4; $i++) {
    $fileName .= rand($DesdeNumero, $HastaNumero);
    $fileName .= chr(rand(ord($DesdeLetra), ord($HastaLetra)));
}

$refCode = $cRef.'-'.$fileName;

$firma = "$ApiKey~$merchantId~$refCode~$cPrice~$currency"; //Cadena a encriptar
$signature = md5($firma); // Encripta firma con MD5

//echo $fileName;
?>

<form action="<?php echo $url ?>" method="POST" id="paymentForm" autocomplete="nope">
    <input name="merchantId" type="hidden" value="<?php echo $merchantId ?>">
    <input name="referenceCode" type="hidden" value="<?php echo $refCode ?>">
    <input name="description" type="hidden" value="<?php echo 'Descarga de PDF -'.$cName ?>">
    <input name="amount" type="hidden" value="<?php echo $cPrice ?>">
    <input name="tax" type="hidden" value="0">
    <input name="taxReturnBase" type="hidden" value="0">
    <input name="signature" type="hidden" value="<?php echo $signature ?>">
    <input name="accountId" type="hidden" value="<?php echo $accountId ?>">
    <input name="currency" type="hidden" value="<?php echo $currency ?>">
    <input name="test" type="hidden" value="<?php echo $test ?>">
    <input name="extra1" type="hidden" value="<?php echo $refCode ?>">
    <input name="responseUrl" type="hidden" value="<?php echo $responseUrl ?>">
</form>