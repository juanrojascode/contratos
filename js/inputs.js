import dataForm from "./custom.js";
import * as Helpers from "./helpers.js";
import * as local_storage from "./localStorage.js";

/**
 * Agrega la clase .is-invalid o .is-valid a los campos
 * @param {Object} i objeto a tratar
 * @param {Boolean} b si es error o no
 * @param {String} m mensaje que se le dará al error
 */
export function addClassValid(i, b, m) {
  let valIfIsSpan = i.next().prop("localName") == "span";
  if (b) {
    i.removeClass("is-invalid");
    i.addClass("is-valid");
    valIfIsSpan ? i.next().remove() : null;
    setTimeout(() => {
      i.removeClass("is-valid");
    }, 1500);
  } else {
    i.addClass("is-invalid");
    if (valIfIsSpan && i.next().hasClass("error")) {
      i.next().html(m); //cambia el mensaje del error
    } else {
      $('<span class="error invalidForm">' + m + "</span>").insertAfter(i); //agrega el error después del campo
    }
  }
}

/**
 * Validaciones de campo para los tipos de datos
 * @param {Object} i objeto a tratar
 */
export function validInput(i) {
  if (i.prop("localName") == "select") {
    if (i.attr("name") == "cBancoEntidad") {
      i.val() == 0
        ? addClassValid(i, false, Helpers.errors["selectBank"])
        : addClassValid(i, true);
    }
  } else {
    let objetName = i.attr("name");
    switch (i.attr("type")) {
      /* case 'text':
          (i.val().length < 3) ? addClassValid(i, false, Helpers.errors['minCaracter']) : addClassValid(i, true);
          break; */
      case "number":
        switch (objetName) {
          case "contratoDiaPago":
            let num = parseInt(i.val());
            num < 1 || num > 31
              ? addClassValid(i, false, Helpers.errors["maxDay"])
              : addClassValid(i, true);
            break;
          case "contratoDuracion":
            let startDate = $('[name="contratoFechaInicio"]').val(),
              endDate = $('[name="contratoFechaFin"]'),
              new_date = moment(startDate, "YYYY-MM-DD").add(i.val(), "months"),
              day = new_date.format("DD"),
              month = new_date.format("MM"),
              year = new_date.format("YYYY"),
              nDate = day + "/" + month + "/" + year;
            endDate.val(nDate);
            break;
        }
        break;
      case "tel":
        i.val().length < 10
          ? addClassValid(i, false, Helpers.errors["minNumber"])
          : addClassValid(i, true);
        break;
      case "email":
        var caract = new RegExp(
          /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/
        );
        !caract.test(i.val(), "DD-MM-YYYY", true)
          ? addClassValid(i, false, Helpers.errors["email"])
          : addClassValid(i, true);
        break;
      case "date":
        let d = moment(i.val());
        !d.isValid()
          ? addClassValid(i, false, Helpers.errors["dateExist"])
          : addClassValid(i, true);
        if (objetName == "contratoFechaFirma") {
          let startDate = $('[name="contratoFechaInicio"]').val(),
            dateFirm = $('[name="contratoFechaFirma"]').val(),
            a = moment(startDate, "DD-MM-YYYY"),
            b = moment(dateFirm, "DD-MM-YYYY");
          b > a
            ? addClassValid(i, false, Helpers.errors["dateMajor"])
            : addClassValid(i, true);
        }
        break;
      default:
        i.val() == ""
          ? addClassValid(i, false, Helpers.errors["required"])
          : addClassValid(i, true);
        break;
    }
  }
}

/**
 * Valida todos los campos
 * antes de pasar a la siguiente página
 */
export function validEmptyFields() {
  let countErrors = 0,
    selectCoarren = $(
      '.tab-content .show select[name="coarrendatario1Select"]'
    );

  if (!selectCoarren.length > 0 || selectCoarren.val() == "s") {
    //Guarda el selector del coarrendatario
    if (selectCoarren.length > 0) {
      console.log("si hay");
      local_storage.saveStorage(
        selectCoarren.attr("name"),
        selectCoarren.val()
      );
    }
    //valida si existe un selec y si el valor es 'si'
    $(".tab-content .show.active .row")
      .children()
      .each(function () {
        //selecciona .col-md-*
        $(this)
          .children()
          .each(function () {
            //Selecciona sus hijos
            let name = $(this).attr("name"), //Nombre del campo
              value = $(this).val(); //Valor del campo

            if (name != null) {
              if (value == "") {
                //Si el campo está vacío alerta al usuario
                validInput($(this));
                ++countErrors; //aumentar un error
                //console.log(countErrors);
              } else {
                //countErrors = 0; //disminuir un error
                //console.log(countErrors);
                validInput($(this));
                local_storage.saveStorage(name, value); //Guarda los datos en el localStorage
              }
            }
          });
      });
    //Reemplazar nombres encriptados
    for (let key in dataForm) {
      local_storage.saveStorage(key, dataForm[key]);
    }
    //let exeption = false;
    //Valida si es la última página
    /* if ($('select[name="contratoMedioDePago"]').val() == "e") {
        //exeption = true;
        countErrors = 0;
      } */
    if (countErrors == 0 && $("#progressItem").children("p").html() == "100%") {
      //$("#modalPayment").modal("show");
      Helpers.payment();
    }
  }
  return countErrors;
}

/**
 * Valida el onChange del select para el formulario actual
 * habilitando o no el formulario
 * @param {Object} o Objeto select a tratar
 */
export function validSelectOnChage(o) {
  const html = $("html");
  const thisObject = $(o),
    disableBlock = $(o).parent().next();
  if (
    thisObject.prop("checked") ||
    thisObject.val() == "n" ||
    thisObject.val() == "e"
  ) {
    disableBlock.attr("disabled", true);
    html.scrollTop(1000); //Baja el scroll para facilitar el clic en btnNext;
  } else {
    disableBlock.attr("disabled", false);
  }
}
