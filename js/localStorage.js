export function getStorage(e) {
  return localStorage.getItem(e);
}
export function saveStorage(e, v) {
  localStorage.setItem(e, v);
}
export function removeStorage(e) {
  localStorage.removeItem(e);
}
export function cleanStorage() {
  localStorage.clear();
}
