import * as Helpers from "./helpers.js";
import * as Inputs from "./inputs.js";
import * as local_storage from "./localStorage.js";

const dataForm = [];

export default dataForm;

//Habilita la función para poder utilizarla desde HTML con "onClick"
window.consultDocument = Helpers.consultDocument;
window.validEmptyFields = Inputs.validEmptyFields;
window.validSelectOnChage = Inputs.validSelectOnChage;
window.repreLegal = repreLegal;

//Variables globales
let count = 0,
inputsCount = 0;
const btnBack = $("#btnBack"),
  btnNext = $("#btnNext"),
  btnPayment = $("#btnPayment"),
  oFullName = $(".tab-content .active.show .nombre"),
  html = $("html"),
  repreLegalInputs = `<div class="col-md-6 repreItem">
        <label class="form-label">Tipo de documento del representante legal</label>
        <select name="repreTipoDocumento" class="form-select">
            <option value="cc" selected="">Cédula de Ciudadanía</option>
            <option value="ce">Cédula de extranjería</option>
            <option value="pep">Permiso especial de permanencia</option>
        </select>
      </div>
      <div class="col-md-6 repreItem">
        <label class="form-label" for="repreNumeroDocumento">Número de documento del representante legal</label>
        <input type="number" name="repreNumeroDocumento" class="form-control" onblur="consultDocument(this)">
      </div>
      <div class="col-md-6 repreItem">
        <label class="form-label" for="repreNombre">Nombre del representante legal</label>
        <input type="text" name="repreNombre" class="form-control" disabled>
      </div>`,
  repreLegal2Inputs = `<div class="col-md-6 repreItem">
        <label class="form-label">Tipo de documento del representante legal</label>
        <select name="repre2TipoDocumento" class="form-select">
            <option value="cc" selected="">Cédula de Ciudadanía</option>
            <option value="ce">Cédula de extranjería</option>
            <option value="pep">Permiso especial de permanencia</option>
        </select>
      </div>
      <div class="col-md-6 repreItem">
        <label class="form-label" for="repre2NumeroDocumento">Número de documento del representante legal</label>
        <input type="number" name="repre2NumeroDocumento" class="form-control" onblur="consultDocument(this)">
      </div>
      <div class="col-md-6 repreItem">
        <label class="form-label" for="repre2Nombre">Nombre del representante legal</label>
        <input type="text" name="repre2Nombre" class="form-control" disabled>
      </div>`;

/**
 * Iniciar
 */
document.addEventListener("DOMContentLoaded", function () {
  btnBack.hide();
  btnPayment.hide();
  local_storage.cleanStorage();
  //searchFunction();
  updateInputs();
  keyupFunction();
  datapickerInit();
});

function datapickerInit() {
  $("input[type=date]").datepicker({
    dateFormat: "yy-mm-dd",
    monthNamesShort: [
      "Ene",
      "Feb",
      "Mar",
      "Abr",
      "May",
      "Jun",
      "Jul",
      "Ago",
      "Sep",
      "Oct",
      "Nov",
      "Dic",
    ],
    changeMonth: true,
    changeYear: true,
  });
}

/**
 * - Validacion al soltar la tecla
 * - Deshabilita clic derecho
 * - Deshabilita ctrl+v
 * - Consulta de nombre en repre legal
 * - Desabilita caracter . en inputs number y tel
 */
function keyupFunction() {
  $("input, select").change(function () {
    if ($(this).next().prop("localName") == "span") {
      $(this).val() != "" ? Inputs.validInput($(this)) : null;
    } else {
      $(this).val().length > 0 ? Inputs.validInput($(this)) : null;
    }
  });
  /* $("select").change(function () {
    if ($(this).next().prop("localName") == "span") {
      $(this).val() != "" ? Inputs.validInput($(this)) : null;
    } else {
      $(this).val().length > 0 ? Inputs.validInput($(this)) : null;
    }
  }); */
  /* $('input[name="contratoDiaPago"]').keyup(function () {
    Inputs.validInput($(this))
  }); */
  $(html).on("contextmenu", function (e) {
    return false;
  });
  $(html).on("paste", function (e) {
    return false;
  });
  $('input[type="number"], input[type="tel"]').keypress(function (e) {
    if (e.charCode < 48 || e.charCode > 57) {
      return false;
    }
  });
}

/**
 * Encriptador de nombres
 * @param {String} name
 */
function encriptName(name, obj) {
  let nameInput = $(obj).attr("name"),
    fullNameEncript = "",
    asterisk = "",
    result = "",
    words = name.split(" ");
  names[nameInput] = name;
  //saveStorage(nameInput, name);
  words.forEach((ele) => {
    firstChar = ele.charAt(0);
    //lastChar = ele.charAt(ele.length - 1);
    for (let i = 1; i < ele.length; i++) {
      asterisk += "*";
    }
    result = firstChar + asterisk + " ";
    asterisk = "";
    fullNameEncript += result;
  });
  return fullNameEncript;
}

/*
 * Acción buscador
 */
function searchFunction() {
  let menu = $("#block-menuprincipal"),
    iconSearch = $("#search-trigger"),
    searchForm = $("#search-form");

  iconSearch.on("click", function () {
    menu.addClass("d-none");
    searchForm.removeClass("d-none");
  });
  $("#closeSearch").on("click", function () {
    menu.removeClass("d-none");
    searchForm.addClass("d-none");
  });
}

/**
 * Avanza la vista
 */
btnNext.click(() => {
  let list = $(".list-group > .active").next("a"),
    listContent = $(".tab-content > .show.active").next("div");
  html.scrollTop(250);
  if (validEmptyFields() == 0) {
    //Valida si no hay errores
    backNext(list, listContent); //Pasa a la siguiente página
    btnBack.show();
    count++; //Aumenta el conteo para la barra progesiva
    updateProgressBar(); //Actualiza la barra de progreso
  }
  //getValueInput();
});

/**
 * Retrocede la vista
 */
btnBack.click(() => {
  let list = $(".list-group > .active").prev("a"),
    listContent = $(".tab-content > .show.active").prev("div");
  backNext(list, listContent); //cambia la vista actual por la anterior
  btnNext.show(); //Muestra el btnNext
  count--; //Disminuye el conteo para la barra progesiva
  updateProgressBar(); //Actualiza la barra de progreso
});

/**
 * Funcion para realizar el avance o retroceso de las pestañas
 * teniendo en cuenta el elemento actual y si hay más o no elementos.
 * @param {Object} l list
 * @param {Object} lc list content
 */
function backNext(l, lc) {
  if (l.prev("a").length == 0) {
    //Si no hay más elementos al retroceder, esconde el btnBack
    btnBack.hide();
  }
  if (l.length > 0) {
    //Muestra el siguiente elemento
    $(".list-group > .active").removeClass("active");
    $(".tab-content > .show.active").removeClass("show active");
    l.addClass("active").removeClass("disabled");
    lc.addClass("show active");
  }
}

function updateInputs() {
  let a = $(".list-group a").length;
  a--;
  inputsCount = a;
}

/**
 * Actualiza el progreso del usuario
 */
function updateProgressBar() {
  updateInputs();
  let pItem = $("#progressItem").children("div"),
    pItemTitle = $("#progressItem").children("p"),
    t = Math.floor((count / inputsCount) * 100);

  pItem.attr("style", "width: " + t + "%");
  pItemTitle.html(t + "%");

  if (pItemTitle.html() == "100%") {
    btnNext.hide();
    btnPayment.show();
    pItem.addClass("bg-success");
  } else {
    btnPayment.hide();
    pItem.hasClass("bg-success") ? pItem.removeClass("bg-success") : null;
  }
}

/**
 * Activa o desactiva el representate legal en el formulario
 * @param {Objet} o select
 */
export function repreLegal(o) {
  let selectValue = $(o).val(),
    parentObject = $(".active.show .nombre").parent(),
    addresInput = $(".active.show .row").children();
  if (selectValue == "nit") {
    $(repreLegalInputs).insertAfter(parentObject);
    $(".active.show .labelRepre").show();
    $(addresInput[5]).attr("class", "col-md-6");
  } else {
    $(".active.show .repreItem").remove();
    $(".active.show .labelRepre").hide();
    $(addresInput[8]).attr("class", "col-md-12");
  }
}
