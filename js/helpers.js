/**
 * CONTENIDO DEL ARCHIVO
 * - errors: Contiene la lista de mensajes de error a mostrar en la aplicación
 * - Loader, habilita o deshabilita el loader en el botón consult
 * - Popups con Sweet alerts
 * - consultDocument, consulta en la API por documento o NIT el nombre
 * -
*/

import * as Inputs from "./inputs.js";

// Arreglo que contiene los errores a mostrar en los campos.
export let errors = {
  required: "Este campo es requerido.",
  minCaracter: "El nombre debe contener al menos 3 caracteres.",
  minNumber: "El teléfono debe contener al menos 10 dígitos.",
  email: "El correo ingresado debe tener un formato correo@dominio.com.",
  maxDay: "El número no puede ser mayor a 31.",
  dateExist: "La fecha introducida no es válida.",
  dateMajor: "La fecha debe ser anterior o igual al día de inicio del contrato.",
  selectBank: "Seleccione un banco por favor."
};

/**
 * Activar o desactivar el loader
 */
export function loader(value) {
  const btn = $(".btnConsult");
  const spinnerElement = `<div class="spinner spinner-border text-primary" role="status"><span class="visually-hidden">Loading...</span></div>`;
  if (value) {
    $(btn).attr("disabled", true);
    $(spinnerElement).insertAfter(btn);
  } else {
    $(btn).attr("disabled", false);
    $(".spinner").remove();
  }
}

/**
 * Swal popup message
 * @param {string} icon
 * @param {string} title
 * @param {string} message
 * @param {string} textConfirmButton
 */
export function showSweetAlert(icon, title, message, textConfirmButton) {
  Swal.fire({
    icon: icon,
    title: title,
    text: message,
    confirmButtonText: textConfirmButton,
  });
}

/**
 * API REST url: https://documenter.getpostman.com/view/3987067/SWE56Jz1?version=latest
 * Consulta de nombres con tipo y número de documento en la
 * Registraduría Nacional del Estado Civil
 */
export function consultDocument(o) {
  let tipoDocumento = "",
    onDocumento = "",
    nDocumento = "",
    inputFullName = "";
  //si es null selecciona por default los primeros elemenos selec, input y campo nombre
  if (o == null) {
    //representante legal
    (tipoDocumento = $(".tab-content .active.show .tipoDocumento").val()),
      (onDocumento = $(".tab-content .active.show .nTipoDocumento")),
      (nDocumento = onDocumento.val());
    inputFullName = $(".tab-content .active.show .nombre");
  } else {
    //Toma dinámicamente los campos siguiente (representante legal)
    (tipoDocumento = $(o).parent().prev().children("select").val()),
      (onDocumento = $(o)),
      (nDocumento = onDocumento.val());
    inputFullName = $(o).parent().next().children("input");
  }
  //Valida si el campo nDocumento está vacío
  //Si no, ejecuta la consulta, de lo contrario alerta al usuario.
  if (nDocumento != "") {
    let dataDocument = {
      documentType: tipoDocumento,
      documentNumber: nDocumento.toString(),
    };
    $.ajax({
      url: "app/names",
      dataType: "JSON",
      type: "POST",
      data: dataDocument,
      timeout: 4000,
      beforeSend: function () {
        loader(true);
      },
      success: function (response) {
        let dataResponse = JSON.parse(response),
          statusResquest = dataResponse.code;
        switch (statusResquest) {
          case 200:
            showSweetAlert(
              "success",
              "¡Muy bien!",
              "Hemos encontrado tus datos.",
              "Continuar"
            );
            tipoDocumento == "nit"
              ? inputFullName.val(
                  encriptName(dataResponse.data[0].razonSocial, inputFullName)
                )
              : inputFullName.val(
                  encriptName(dataResponse.data.fullName, inputFullName)
                );
            addClassValid(inputFullName, true);
            break;
          case 404:
            showSweetAlert(
              "error",
              "Lo sentimos",
              "Verifica si escribiste bien el número de documento.",
              "Entendido"
            );
            break;
          default:
            showSweetAlert(
              "error",
              "Lo sentimos",
              "No hemos podido conectar con el servidor, por favor, completa el campo 'Nombre completo' manualmente.",
              "Entendido"
            );
            $(".nombre").prop("disabled", false);
            console.error(dataResponse);
            break;
        }
        loader(false);
      },
      error: function () {
        loader(false);
        showSweetAlert(
          "error",
          "Lo sentimos",
          "No podemos obtener los datos. Completa el campo nombre manualmente, más adelante lo comprobaremos.",
          "Entendido"
        );
        inputFullName.prop("disabled", false);
      },
    });
  } else {
    Inputs.addClassValid(onDocumento, false, errors["required"]); //valida campo documento
  }
}

/**
 * Se encarga de tomar la referencia del contrato actual y envío de datos
 * almacenados en localStorage para generar el PDF y a su vez envíar a la página de pago.
 */
export function payment() {
  const getUrl = window.location.search,
    getUrlParams = new URLSearchParams(getUrl),
    cRef = getUrlParams.get("ref");

  let miStorage = JSON.stringify(window.localStorage),
    nDocument = $("input[name=extra1]").val(); //nombre del contrato y código aleatorio

  $.ajax({
    method: "POST",
    url: "generarpdf",
    data: { sto: miStorage, nDoc: nDocument, ref: cRef },
    success: function (res) {
      $("#paymentForm").submit();
    },
  });
}
