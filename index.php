<?php
$cValid = false;

if ($_GET) { //Valida si la URL tiene parámentros
  if (!empty($_GET['ref'])) { //Verifica si exite el parámetro 'ref'
    if (validCont( $_GET['ref'] )) { //Valida si el parámetro pasado es igual a algún contrato
      $cValid = true;
      include('single-contrato.php'); //Muestra el formulario escogido
    }
  }
}

if (!$cValid) {
  include('home.php'); //Muestra la lista de contratos
}

function validCont( $ele )
{
  include(dirname(__FILE__) . '/config.php');

  $bool = false;

  for ($i = 0; $i < $lengthJson; $i++) {
    if ($readJson[$i]['ref'] == $ele) { //obtener valor iguales a la ref. obtenida
      $bool = true;
    }
  }

  return $bool;
}
