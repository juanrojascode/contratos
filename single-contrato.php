<?php

include(dirname(__FILE__) . '/config.php');

$cRef = $_GET['ref']; // ( 'cont001' ) Referencia del contrato obtenida de URL
$incTemplate = 'templates/' . $cRef . '.html'; //Ruta de la plantilla según contrato

for ($i = 0; $i < $lengthJson; $i++) {
    if ($readJson[$i]['ref'] == $cRef) { //obtener valor iguales a la ref. obtenida
        $cPrice = $readJson[$i]['price']; //Precio del contrato
        $cName = $readJson[$i]['name']; //Nombre del contrato
    }
}

?>
<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include('templates/layout/meta.html') ?>

    <title><?php echo $cName ?> | Abogados Baluarte</title>
</head>

<body>

    <?php include('templates/layout/header.html') ?>

    <div class="container controls">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contrato</li>
            </ol>
        </nav>
        <div class="row justify-content-center">
            <div class="text-center mb-5" id="nameContract">
                <h4 class="mx-auto"><?php echo $cName ?></h4>
            </div>
            <div class="col-12 col-md-10">
                <form action="" method="POST" id="form-content" autocomplete="off">
                    <?php 
                        include_once($incTemplate);
                    ?>
                    <div class="form-btns mx-auto">
                        <button type="button" class="btn btn-primary me-2" id="btnBack">Anterior</button>
                        <button type="button" class="btn btn-primary" id="btnNext">Siguiente</button>
                        <button type="button" class="btn btn-success ms-2" onclick="validEmptyFields()" id="btnPayment">Finalizar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php include('templates/layout/form/formpayment.php') ?>

    <!-- Modal payment -->
    <!-- <div class="modal fade" id="modalPayment" tabindex="-1" aria-labelledby="modalPaymentLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content p-3">
                <div class="modal-body">
                    <?php //include('templates/layout/form/formpayment.php') 
                    ?>
                </div>
            </div>
        </div>
    </div> -->

    <?php include('templates/layout/scripts.html') ?>

</body>

</html>