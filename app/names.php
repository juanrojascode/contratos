<?php

  if ($_POST) {
    $curl = curl_init();
    $apiKey = 'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGllbnRJZCI6IjYxYTkyMmYyMzFjYWQ0NmJmOWFmMjVkZCIsImRvY3VtZW50VHlwZSI6IkNDIiwiZG9jdW1lbnROdW1iZXIiOiIxMDcwNjE1Njg1IiwidiI6MSwicm9sZSI6ImNsaWVudCIsInN1YnNjcmlwdGlvblBsYW4iOiI2MWE5MjJmMjMxY2FkNDZiZjlhZjI1ZGUiLCJpYXQiOjE2Mzg0NzQ5ODh9.4Kctmh1w1L5_TESoCF666TFCdpXHkPSqgdAuv9iqVow';
    $documentType = $_POST['documentType'];
    $documentNumber = $_POST['documentNumber'];

    if ($documentType == 'nit') {
      $apiKey = 'Authorization: ziil6clta3782f3hqj14fsf8s6kr60p1sggr0la6esjbaca6';
      $url = 'https://api.verifik.co/v2/co/rues/consultarEmpresaPorNit';
      $data = 'nit='.$documentNumber;
    }else{
      $url = 'https://api.verifik.co/v2/co/consultarNombres';
      $data = 'documentType='.$documentType.'&documentNumber='.$documentNumber;
    }


    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $data,
      CURLOPT_HTTPHEADER => array(
        $apiKey,
        'Content-Type: application/x-www-form-urlencoded'
      )
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    echo json_encode($response);
  }