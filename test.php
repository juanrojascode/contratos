<?php

$DesdeLetra = "a";
$HastaLetra = "z";
$DesdeNumero = 1;
$HastaNumero = 9;

$letraAleatoria = '';

for ($i=0; $i < 4 ; $i++) {
    $letraAleatoria .= rand($DesdeNumero, $HastaNumero);
    $letraAleatoria .= chr(rand(ord($DesdeLetra), ord($HastaLetra)));
}

echo $letraAleatoria;