<?php

include(dirname(__FILE__) . '/config.php');

?>

<!doctype html>
<html lang="es">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <?php include('templates/layout/meta.html') ?>

  <title>Contratos | Abogados Baluarte</title>

</head>

<body>

  <?php include('templates/layout/header.html') ?>

  <div class="container controls my-5">

    <p>Elabore de forma fácil y ágile contratos con nuestra plataforma.</p>

    <div class="row justify-content-center mt-5">
      <div class="col-12 col-md-8">
        <div class="list-group">
          <?php
          for ($i = 0; $i < $lengthJson; $i++):
          ?>
          <a href="/?ref=<?php echo $readJson[$i]['ref'] ?>" class="list-group-item list-group-item-action"><?php echo $readJson[$i]['name'] ?><span class="badge rounded-pill bg-primary float-end">$ <?php echo number_format($readJson[$i]['price']).' '.$currency ?></span></a>
          <?php endfor; ?>
        </div>
      </div>
    </div>

  </div>

  <?php include('templates/layout/scripts.html') ?>

</body>

</html>