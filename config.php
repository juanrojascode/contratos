<?php
// ******************** DEFINICIÓN DE VARIALES
//
// $test = 1 para test, 0 para producción
// $url = Url a donde apunta el formulario (Test o producción)            
// $ApiKey = Obtener este dato de la cuenta de PayU
// $merchantId = Obtener este dato de la cuenta de PayU
// $accountId = Obtener este dato de la cuenta de PayU
// $currency = Moneda de la transacción
// $responseUrl = URL de respuesta
// $confirmationUrl = URL de confirmación
//
// ********************

$test = '1'; // 
$currency = 'COP';

if ($test == '1') {
    $url = 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu/';
    $ApiKey = '4Vj8eK4rloUd272L48hsrarnUA';
    $merchantId = '508029';
    $accountId = '512321';
    $responseUrl = 'http://contratos.abogadosbaluarte.com/response.php';
    //$confirmationUrl = 'http://testx.tk/payu/confirmacion.php';
} else {
    $url = 'https://checkout.payulatam.com/ppp-web-gateway-payu/';
    $ApiKey = 'AkRf6A2J4ct8sRhxap5uXcUcSu';
    $merchantId = '899281';
    $accountId = '905909';
    $responseUrl = 'http://contratos.abogadosbaluarte.com/response.php';
    //$confirmationUrl = 'http://testx.tk/payu/confirmacion.php';
}

/**
 * Lectura y conversión de archivo JSON
 * Con la información de los contratos
 */

$fileJson = file_get_contents("contratos.json"); //Leer json contratos
$fileJsonBank = file_get_contents("bancos.json"); //Leer json bancos
$readJson = json_decode($fileJson, true); //Decodificar JSON
$readJsonbank = json_decode($fileJsonBank, true); //Decodificar JSON
$lengthJson = count($readJson); //Obtener tamaño del JSON leído
$lengthJsonBank = count($readJsonbank); //Obtener tamaño del JSON leído