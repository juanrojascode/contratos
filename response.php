<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include('templates/layout/meta.html') ?>

    <title>Contratos | Abogados Baluarte</title>

    <style>
        table td:first-child{
            text-align: right !important;
            font-weight: bold;
        }
    </style>

</head>

<body>
    <?php

    include('templates/layout/header.html');

    include(dirname(__FILE__) . '/config.php');

    $ApiKeyL = $ApiKey;
    $merchant_id = $_REQUEST['merchantId'];
    $referenceCode = $_REQUEST['referenceCode'];
    $TX_VALUE = $_REQUEST['TX_VALUE'];
    $New_value = number_format($TX_VALUE, 1, '.', '');
    $currency = $_REQUEST['currency'];
    $transactionState = $_REQUEST['transactionState'];
    $firma_cadena = "$ApiKeyL~$merchant_id~$referenceCode~$New_value~$currency~$transactionState";
    $firmacreada = md5($firma_cadena);
    $firma = $_REQUEST['signature'];
    $reference_pol = $_REQUEST['reference_pol'];
    $cus = $_REQUEST['cus'];
    $extra1 = $_REQUEST['description'];
    $nArchivo = $_REQUEST['extra1'];
    $pseBank = $_REQUEST['pseBank'];
    $lapPaymentMethod = $_REQUEST['lapPaymentMethod'];
    $transactionId = $_REQUEST['transactionId'];
    $mostrarLink = false;
    $color = '';

    if ($_REQUEST['transactionState'] == 4) {
        $estadoTx = "Transacción aprobada";
        $mostrarLink = true;
        $color = 'success';
    } else if ($_REQUEST['transactionState'] == 6) {
        $estadoTx = "Transacción rechazada";
        $color = 'danger';
    } else if ($_REQUEST['transactionState'] == 104) {
        $estadoTx = "Error";
        $color = 'danger';
    } else if ($_REQUEST['transactionState'] == 7) {
        $estadoTx = "Transacción pendiente";
        $color = 'warning';
    } else {
        $estadoTx = $_REQUEST['mensaje'];
    }


    if (strtoupper($firma) == strtoupper($firmacreada)) {
    ?>
    <div class="container controls my-5">
        <p class="mb-2 text-muted text-center">Resumen Transacción</p>
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                <div class="card">
                    <div class="card-header text-center bg-<?php echo $color ?> text-light"><?php echo $estadoTx; ?></div>
                    <div class="card-body">
                        <table class="table table-striped mx-auto">
                            <tr>
                                <td>ID de la transaccion</td>
                                <td><?php echo $transactionId; ?></td>
                            </tr>
                            <tr>
                                <td>Referencia de la venta</td>
                                <td><?php echo $reference_pol; ?></td>
                            </tr>
                            <tr>
                                <td>Referencia de la transaccion</td>
                                <td><?php echo $referenceCode; ?></td>
                            </tr>
                            <?php
                            if ($pseBank != null):
                            ?>
                            <tr>
                                <td>cus </td>
                                <td><?php echo $cus; ?> </td>
                            </tr>
                            <tr>
                                <td>Banco </td>
                                <td><?php echo $pseBank; ?> </td>
                            </tr>
                            <?php
                            endif;
                            ?>
                            <tr>
                                <td>Valor total</td>
                                <td>$<?php echo number_format($TX_VALUE).' '.$currency; ?></td>
                            </tr>
                            <tr>
                                <td>Descripción</td>
                                <td><?php echo ($extra1); ?></td>
                            </tr>
                            <tr>
                                <td>Entidad</td>
                                <td><?php echo ($lapPaymentMethod); ?></td>
                            </tr>
                        </table>
                        <?php
                        if ($mostrarLink):
                        ?>
                        <div class="download text-center">
                            <a href="public/<?php echo $nArchivo ?>.pdf" class="btn btn-primary mx-auto">Descargar contrato</a>
                        </div>
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
    } else {
    ?>
        <h1>Error validando firma digital.</h1>
    <?php
    }
    ?>

    </div>

    <?php include('templates/layout/scripts.html') ?>

</body>

</html>